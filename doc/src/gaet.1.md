gaet(1) -- gene-based assembly evaluation tool
==============================================

## SYNOPSIS

`gaet` [--reference=<reference-gff>] [--output=<gaet_metrics.yml>] <assembly-gff>

## DESCRIPTION

Given a GFF file for an assembled genome, `gaet` will provide metrics that may
be used to evaluate the completeness of a genome assembly by determining which
genes are present, in what length, and quantity. If a reference GFF file is
additionally provided, further metrics will be generated using set-based
comparisons between the two groups of annotations. See `gaet\_format(5)` for a
description of the output format and metrics produced.

## OPTIONS

  * `-r`, `--reference=<reference-gff>`:

    A GFF file containing annotations for a reference genome. This is the true
    or expected set of annotations for the assembly given by `<assembly-gff>`.

  * `-o`, `--output=<gaet_metrics.yml>`:

    Output file path to store generated metrics.

  * `<assembly-gff>`:

    A GFF file containing annotations for a genome assembly. This assembly will
    be evaluated based on the annotations found in this file.

## SEE ALSO

The tool `checkm analyze` provides complementary functionality whereby the
presence of single copy genes used to determine the quality of a isolate genome
or metagenome bin.
