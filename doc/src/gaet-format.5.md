gaet-format(5) -- Output file format for `gaet`
==============================================

## SYNOPSIS

The `gaet` command returns a YAML-formatted document containing all the metrics
calculated from the input <assembly-gff> file and the optionally provided
<reference-gff> file.

## DESCRIPTION

When `gaet` is run with a GFF file from an assembly, the following group of
metrics will be calculated:

  * minimum_gene_set - minimum sets of genes, such as the minimum required tRNA
    codons for all 20 amino acids, or the minimum gene set for the rRNA operon.
  * gene_type_size - commonly used genome assembly metrics for groups of genes
    such as all tRNA genes. Examples of the calculated metrics are n50 or l50
    for all the tRNA genes.
  * gene_copy_size - commonly used genome assembly metrics for all copies of
    the same gene product, such as 16s rRNA. Examples of the calculated metrics
    are n50 or l50 for all the genes with 16S rRNA listed as the product.

The genes groups which `gene_type_size` metrics are calculated are defined by
their value in the third column of the GFF. These GFF3 fields used are:

  * CDS (protein\_coding)
  * tRNA
  * rRNA
  * tmRNA
  * repeat_region
  * all (the combination of all the above fields)

N.B. This deliberately excludes annotation features with the type `gene` in the
GFF. This is because for almost all cases there is one-to-one relationship
between a `gene` entry and one of types above. Including 'gene' features would
therefore doubles the size of all calculated metrics.

### SIZE

When given only an `<assembly-gff>` file the following metrics will be
calculated for each of the above listed gene groups, and each group of gene
with the same product. For example the below metrics would be calculated for
all rRNA genes, and also for each copy of a 16S rRNA found in the GFF.

  * **sum\_length**: The sum of lengths for all genes in the given group.

  * **count**: The number of genes in the given group.

  * **n50**: The smallest gene length (n) from the assembly at which the the
    sum of all length genes of length >= n, is at least half the sum of all the
    gene lengths.

  * **l50**: The smallest number of genes from the size-ordered list of genes
    from the assembly at which the sum length of these genes is at least half
    the sum of all the gene lengths from the assembly.

When given the `--reference` flag, additional metrics are calculated from the
difference between the reference metrics subtracted from corresponding assembly
metrics. Therefore negative values are possible where the reference has a
greater count or longer genes than the assembly. In addition, the following
metrics are calculated using the length of the corresponding reference genes:

  * **ng50**: The smallest gene length (n) from the assembly at which the the
    sum of all length genes of length >= n, is at least half the sum of all the
    lengths **from the reference**.

  * **lg50**: The smallest number of genes from the size-ordered list of genes
    from the assembly at which the sum length of these genes is at least half
    the sum of all the gene lengths **from the reference**.

### MINIMUM GENE SET

The following `minimum\_gene\_set` gene-type specific metrics will also be
calculated:

  * **tRNA**: are the minimum set of all 20 amino acid coding tRNAs identified.

  * **bacterial archeal rRNA**: are a commonly required set of bacterial and
    archeal rRNA genes identified within the assembly. This is based on the
    following genes:

    * RF00001 : 5S universal large subunit rRNA
    * RF00177 : Bacteria 16S small subunit rRNA
    * SILVA-LSU-Bac / SILVA-LSU-Arc : Bacteria / Archea 23S large subunit rRNA

  * **eukaryotic rRNA**: are the minimum required set of eukaryotic rRNA genes
    identified within the assembly. This is based on the following genes:

    * RF00001 : 5S universal large subunit rRNA
    * RF00002 : 5.8S Eukarya/Archaea large subunit rRNA
    * RF01960 : Eukarya 18S small subunit rRNA
    * SILVA-LSU-Euk : Eukarya 28S large subunit rRNA

  * **single copy**: Single copy genes that are expected to appear once in an
    assembly. This currently consists of:

    * RF00169 / RF01854 / RF01857 : Signal recognition particle
    * RF00010 / RF00011 / RF00373 : Ribonuclease P

### GENE SET DISTANCE

When given the `--reference` flag, the following metrics are calculated for
`gene_type_distance` and `gene_copy_distance`:

  * **n\_intersect**: The number of unique gene sequences found in the both the
    assembly and the reference.

  * **n\_assembly\_only**: The number of unique gene sequences found in only
    the assembly GFF.

  * **n\_reference\_only**: The number of unique gene sequences found in only
    the reference GFF.

  * **n\_symmetric\_difference**: The number of unique gene sequences found in
    only the reference GFF or only the assembly GFF, but not both.

  * **perc\_intersect**: The number of unique gene sequences found in the both
    the assembly and the reference, divided by the union of all unique gene
    sequences.

  * **perc\_assembly\_only**: The number of unique gene sequences found in only
    the assembly GFF, divided by the union of all unique gene sequences.

  * **perc\_reference\_only**: The number of unique gene sequences found in
    only the reference GFF, divided by the union of all unique gene
    sequences.

  * **perc\_symmetric\_difference**: The number of unique gene sequences found
    in only the reference GFF or only the assembly GFF, but not both, divided
    by the union of all unique gene sequences. This is the Jaccard index.

  * **l1_norm**: The sum of absolute differences between the frequency of each
    GFF entry in the reference and the assembly. This is the Manhattan
    distance.

  * **l2_norm**: The square root of the sum of squared differences between the
    frequency of each GFF entry in the reference and the assembly. This is the
    Euclidean distance. The returned value is rounded to 3 decimal places.

The number of shared gene matches between the reference and the assembly are
calculated from the SHA256 digest of the gene sequences provided in the GFF
FASTA. The gene sequence is reverse complemented before digest if it appears on
the negative strand. The SHA256 digests from the assembly and the reference
GFFs are used to calculate the intersect, difference and symmetric difference
metrics. As these are set operations, the unique digests of each gene sequence
are used.

The L1 and L2 norms are calculated for using frequency of each gene SHA256
digest in the reference and the assembly. I.e. the presence of two copies of
the *i*th gene in the assembly and none in the reference would mean the *i*th
values in each annotation set's respective vectors would be 2 and 0. The L1 and
L2 norms are then calculated as the distance between these two vectors.

The `comparison.gene_set_agreement` metrics are calculated from the `XNOR`
comparison of the `minimum_gene_set` metrics in the assembly and the reference.
I.e. if the `minimum_gene_set.tRNA` in both the reference and the assembly have
the same Boolean value the `gene_set_agreement.tRNA` value with be true,
otherwise false.

### NOTE ON DISTANCE MEASURES

The intersect, symmetric difference and related metrics are set operations and
therefore the frequency of the each unique gene sequence is not included as
part of the calculation. When comparing the reference and an assembly these are
useful for examining how many annotations are specific to either the reference
or the assembly. An example if the reference has many more unique rRNA
sequences it likely means that the contigs containing these rRNA are likely the
result of the reads containing these genes collapsing down to a single copy
during assembly.

The L1 norm distance increases linearly with diverging gene copies between the
reference and the assembly. Therefore this measure is more useful where a
greater number of mismatching sequences is expected, such as comparing all
genes or all CDS encoding genes between assemblies.

The L2 norm distance increases quadratically with diverging gene copies between
the reference and the assembly. This distance measure is therefore more useful
where diverging sequences should be penalised more heavily. Examples of where
this would be useful are the tRNA or rRNA gene groups, as errors in these
sequences may cause downstream problems in phylogenetic analysis.

### NOTE ON PARTIAL GENES

Annotation pipelines such as barrnap may report gene products as "16S ribosomal
rRNA (partial)" if they are under a certain length. `gaet` will include all
genes matching the product names even in they include the "(partial)" string.
This is because GAET does not try to determine what constitutes a full length
gene sequence, and whether or not they should be included when calculating
metrics. Furthermore, not all annotation pipelines are guaranteed to use
"(partial)" when annotating short sequences. The user is therefore instead
encouraged to use metrics such as the sum of gene lengths, n50 or comparison to
the reference annotation lengths to determine the quality of the annotated
sequences.

## EXAMPLE

An example subset output from `gaet` comparing a reference and the assembly is
provided:

    ---
    assembly:
      gene_type_size:
        all:
          sum_length: 100
          count: 5
          n50: 60
          l50: 3
        tRNA:
          sum_length: 10
          count: 1
          n50: 10
          l50: 1
      gene_copy_size:
        16S_rRNA
          sum_length: 1000
          count: 3
          n50: 60
          l50: 2
        val_tRNA:
          sum_length: 500
          count: 5
          n50: 10
          l50: 1
      minimum_gene_set:
        tRNA: false
        bacteria_archea_rRNA: true
        eukarya_rRNA: false
    comparison:
      gene_copy_size:
        16S_rRNA
          sum_length: -100
          count: -3
          n50: -60
          l50: -2
          ng50: 600
          lg50: 15
      gene_type_distance:
        all:
          n_intersect: 20
          n_assembly_only: 4
          n_reference_only: 5
          n_symmetric_difference: 9
          l1_norm: 6
          l2_norm: 6
        tRNA:
          n_intersect: 5
          n_assembly_only: 1
          n_reference_only: 2
          n_symmetric_difference: 3
          l1_norm: 6
          l2_norm: 6
      gene_set_agreement:
        tRNA: false
        bacteria_archea_rRNA: true
        eukarya_rRNA: true
