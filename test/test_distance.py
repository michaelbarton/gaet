from math import sqrt

import gaet.metrics.distance as dist

from gaet.types import Gene

def test_distance_metrics():
    gene = lambda digest: Gene('', '', '', '', digest)
    asm = [gene(1), gene(2)]
    ref = [gene(1), gene(3), gene(4)]
    expected = {'n_intersect'               : 1,
                'n_assembly_only'           : 1,
                'n_reference_only'          : 2,
                'n_symmetric_difference'    : 3,
                'perc_intersect'            : 1/4.0,
                'perc_assembly_only'        : 1/4.0,
                'perc_reference_only'       : 2/4.0,
                'perc_symmetric_difference' : 3/4.0,
                'l1_norm'                   : 3,
                'l2_norm'                   : round(sqrt(3), 3)}
    assert dist.distance_metrics(asm, ref) == expected
