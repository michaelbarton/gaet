import pytest
import gaet.util as util

def test_command_line_args_without_ref():
    parsed = util.parse_args(["assembly", "--output=output"])
    assert parsed == {"reference" : None, "assembly" : "assembly", "output" : "output"}


def test_command_line_args_without_ref():
    parsed = util.parse_args(["--reference=ref", "--output=output", "assembly"])
    assert parsed == {"reference" : "ref", "assembly" : "assembly", "output" : "output"}


def test_destination():
    args = {"reference" : "ref", "assembly" : "assembly.gff", "output" : "output"}
    assert util.destination(args) == "output"


def test_destination_when_not_specified():
    args = {"assembly" : "assembly.gff"}
    assert util.destination(args) == "gaet_metrics.yml"
