from random import shuffle

import pytest
import zoo_helper        as zoo
import gaet.metrics.size as size

def test_n50():
    sizes = [91, 77, 70, 69, 62, 56, 45, 29, 16, 4]
    shuffle(sizes)
    assert size.nx(sizes, sum(sizes) / 2.0) == 69
    assert size.nx(sizes, 160) == 77

def test_l50():
    sizes = [91, 77, 70, 69, 62, 56, 45, 29, 16, 4]
    shuffle(sizes)
    assert size.lx(sizes, sum(sizes) / 2.0) == 4
    assert size.lx(sizes, 160) == 2

def test_singleton_metric_group():
    genes = zoo.create_genes('trna', 'incomplete_set')
    expected = {"sum_length" : 120, "count" : 2, "n50" : 60, "l50" : 1}
    assert size.group_size(genes) == expected

def test_comparison_metric_group():
    asm = zoo.create_genes('trna', 'incomplete_set')
    ref = zoo.create_genes('trna', 'increasing_length')
    expected = {"ng50" : None, "lg50" : None, "sum_length" : -180, "count" : -2, "n50" : -20, "l50" : -1}
    assert size.group_size_difference(asm, ref) == expected
