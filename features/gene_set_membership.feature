Feature: Assessing the quality of an assembly gene set comparisions

  Scenario Outline: Computing gene set membership metrics for the reference and assembly
    Given I create a "<file>" gff entry file "assembly.gff"
      And I create a "<file>" gff entry file "reference.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "assembly/minimum_gene_set" should have the following:
         | tRNA                 | <tRNA>                 |
         | bacteria_archea_rRNA | <bacteria_archea_rRNA> |
         | eukarya_rRNA         | <eukarya_rRNA>         |
         | single_copy          | <single>               |
      And the JSON at "reference/minimum_gene_set" should have the following:
         | tRNA                 | <tRNA>                 |
         | bacteria_archea_rRNA | <bacteria_archea_rRNA> |
         | eukarya_rRNA         | <eukarya_rRNA>         |
         | single_copy          | <single>               |

    Examples:
      | file                                        | bacteria_archea_rRNA | eukarya_rRNA | tRNA  | single |
      | trna/incomplete_set                         | false                | false        | false | false  |
      | trna/complete_set                           | false                | false        | true  | false  |
      | trna/complete_set_with_partial              | false                | false        | true  | false  |
      | rrna/bacteria_complete_set                  | true                 | false        | false | false  |
      | rrna/bacteria_complete_set_with_partial     | true                 | false        | false | false  |
      | rrna/bacteria_incomplete_set_with_partial   | false                | false        | false | false  |
      | single_copy/name_set_1                      | false                | false        | false |   true |
      | single_copy/name_set_2                      | false                | false        | false |   true |
      | single_copy/name_set_3                      | false                | false        | false |   true |


  Scenario Outline: Computing gene set agreement between the reference and assembly
    Given I create a "<assembly>" gff entry file "assembly.gff"
      And I create a "<reference>" gff entry file "reference.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "comparison/gene_set_agreement" should have the following:
         | tRNA                 | <tRNA> |
         | bacteria_archea_rRNA | <rRNA> |
         | eukarya_rRNA         | true   |

    Examples:
      | assembly                                | reference                  | rRNA  | tRNA  |
      | trna/incomplete_set                     | trna/incomplete_set        | true  | true  |
      | trna/incomplete_set_with_partial        | trna/incomplete_set        | true  | true  |
      | trna/complete_set                       | trna/incomplete_set        | true  | false |
      | trna/incomplete_set_with_partial        | trna/complete_set          | true  | false |
      | rrna/bacteria_complete_set_with_partial | rrna/bacteria_complete_set | true  | true  |
      | trna/incomplete_set                     | rrna/bacteria_complete_set | false | true  |
      | rrna/bacteria_complete_set              | trna/complete_set          | false | false |
      | trna/complete_set                       | rrna/bacteria_complete_set | false | false |