Feature: Assessing the sizes of gene groups using `gaet`

  Scenario Outline: Assessing size metrics for an assembly
    Given I create a "<file>" gff entry file "assembly.gff"
     When I run `gaet assembly.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "assembly/gene_type_size/<group>" should have the following:
        | sum_length | 120 |
        | count      | 2   |
        | n50        | 60  |
        | l50        | 1   |

    Examples:
      | file                                      | group         |
      | trna/incomplete_set                       | all           |
      | trna/incomplete_set                       | tRNA          |
      | trna/incomplete_set_with_partial          | all           |
      | trna/incomplete_set_with_partial          | tRNA          |
      | rrna/bacteria_incomplete_set              | rRNA          |
      | rrna/bacteria_incomplete_set_with_partial | rRNA          |
      | repeat/two                                | repeat_region |
      | cds/two                                   | CDS           |
      | cds/two_with_genes                        | CDS           |
      | cds/two_with_genes                        | all           |
      | mixed/two_cds_with_unknown                | all           |
      | tmrna/two                                 | all           |
      | tmrna/two                                 | tmRNA         |


  Scenario Outline: Assessing reference-based size metrics for an assembly
    Given I create a "<assembly>" gff entry file "assembly.gff"
      And I create a "<reference>" gff entry file "reference.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "comparison/gene_type_size/<group>" should have the following:
        | sum_length | <sum>   |
        | count      | <count> |
        | n50        | <n50>   |
        | l50        | <l50>   |
        | ng50       | <ng50>  |
        | lg50       | <lg50>  |

    Examples:
      | assembly            | reference              | group | sum   | count | n50 | l50 | ng50 | lg50 |
      | trna/incomplete_set | trna/incomplete_set    | all   | 0     | 0     | 0   | 0   | 60   | 1    |
      | trna/incomplete_set | trna/complete_set      | all   | -1080 | -18   | 0   | -9  | null | null |
      | trna/incomplete_set | trna/increasing_length | tRNA  | -180  | -2    | -20 | -1  | null | null |


  Scenario Outline: Assessing reference-based size metrics for an assembly when they cannot be calculated
    Given I create a "trna/incomplete_set" gff entry file "assembly.gff"
      And I create a "trna/complete_set" gff entry file "reference.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "comparison/gene_type_size/<group>" should have the following:
         | ng50   | null |
         | lg50   | null |

    Examples:
      | group         |
      | all           |
      | tRNA          |
      | rRNA          |
      | repeat_region |
      | CDS           |