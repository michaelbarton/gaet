Feature: Assessing the quality of an assembly using gene set distance comparisions

  Scenario Outline: Computing gene type distance metrics
    Given I create a "trna/incomplete_set" gff entry file "assembly.gff"
      And I create a "<reference>" gff entry file "reference.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "comparison/gene_type_distance/<group>" should have the following:
         | n_intersect               | <n_intr>  |
         | n_assembly_only           | <n_asm>   |
         | n_reference_only          | <n_ref>   |
         | n_symmetric_difference    | <n_symm>  |
         | perc_intersect            | <p_intr>  |
         | perc_assembly_only        | <p_asm>   |
         | perc_reference_only       | <p_ref>   |
         | perc_symmetric_difference | <p_symm>  |
         | perc_symmetric_difference | <p_symm>  |

    Examples:
      | reference           | group | n_intr | n_asm | n_ref | n_symm | p_intr | p_asm | p_ref | p_symm |
      | trna/incomplete_set | all   | 2      | 0     | 0     | 0      | 1.0    | 0.0   | 0.0   | 0.0    |
      | trna/incomplete_set | tRNA  | 2      | 0     | 0     | 0      | 1.0    | 0.0   | 0.0   | 0.0    |
      | trna/incomplete_set | CDS   | 0      | 0     | 0     | 0      | null   | null  | null  | null   |
      | cds/two             | all   | 0      | 2     | 2     | 4      | 0.0    | 0.5   | 0.5   | 1.0    |
      | cds/two             | tRNA  | 0      | 2     | 0     | 2      | 0.0    | 1.0   | 0.0   | 1.0    |
      | cds/two             | CDS   | 0      | 0     | 2     | 2      | 0.0    | 0.0   | 1.0   | 1.0    |


  Scenario Outline: Computing L1/L2 gene type distance metrics
    Given I create a "<assembly>" gff entry file "assembly.gff"
      And I create a "<reference>" gff entry file "reference.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "comparison/gene_type_distance/all" should have the following:
         | l1_norm  | <all_l1> |
         | l2_norm  | <all_l2> |
      And the JSON at "comparison/gene_type_distance/<group>" should have the following:
         | l1_norm  | <sub_l1> |
         | l2_norm  | <sub_l2> |

    Examples:
      | assembly            | reference            | group  | all_l1 | all_l2 | sub_l1 | sub_l2 |
      | trna/incomplete_set | trna/incomplete_set  | tRNA   | 0      | 0.0    | 0      | 0.0    |
      | cds/two             | cds/two              | CDS    | 0      | 0.0    | 0      | 0.0    |
      | trna/alanine_ggc    | trna/two_alanine_ggc | tRNA   | 1      | 1.0    | 1      | 1.0    |
      | cds/two             | trna/two_alanine_ggc | tRNA   | 4      | 2.449  | 2      | 2.0    |
      | trna/four_valine_gtt | trna/alanine_ggc    | tRNA   | 5      | 4.123  | 5      | 4.123  |
