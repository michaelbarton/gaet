Feature: Assessing the quality of realistic assemblies using `gaet`

  Scenario Outline: Assessing realistic annotation data for the assembly GFF
    Given I copy the file "../../test/data/<file>.gff" to "assembly.gff"
     When I run `gaet assembly.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document

    Examples:
      | file     |
      | archea   |
      | bacteria |


  Scenario: Assessing realistic annotation data for the reference GFF
    Given I copy the file "../../test/data/archea.gff" to "reference.gff"
      And I copy the file "../../test/data/bacteria.gff" to "assembly.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
