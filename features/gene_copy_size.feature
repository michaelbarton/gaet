Feature: Assessing the sizes of gene copies using `gaet`

  Scenario Outline: Assessing gene copy size metrics for an assembly
    Given I create a "<file>" gff entry file "assembly.gff"
     When I run `gaet assembly.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "assembly/gene_copy_size/<gene>" should have the following:
        | sum_length | <sum>   |
        | count      | <count> |
        | n50        | <n50>   |
        | l50        | <l50>   |

    Examples:
      | file                             | gene     | sum | count | n50  | l50  |
      | trna/incomplete_set_with_partial | arg_tRNA | 60  | 1     | 60   | 1    |
      | trna/incomplete_set_with_partial | val_tRNA | 0   | 0     | null | null |
      | trna/two_alanine_ggc             | ala_tRNA | 120 | 2     | 60   | 1    |


  Scenario Outline: Assessing reference-based size metrics for an assembly
    Given I create a "<assembly>" gff entry file "assembly.gff"
      And I create a "<reference>" gff entry file "reference.gff"
     When I run `gaet assembly.gff --reference=reference.gff --output=metrics.yml`
     Then the stderr should not contain anything
      And the stdout should not contain anything
      And the exit status should be 0
      And the file "./metrics.yml" should exist
      And the file "./metrics.yml" should be a valid YAML document
      And the JSON at "comparison/gene_copy_size/<gene>" should have the following:
        | sum_length | <sum>   |
        | count      | <count> |
        | n50        | <n50>   |
        | l50        | <l50>   |
        | ng50       | <ng50>  |
        | lg50       | <lg50>  |

    Examples:
      | assembly            | reference            | gene     | sum   | count | n50 | l50 | ng50 | lg50 |
      | trna/incomplete_set | trna/incomplete_set  | arg_tRNA | 0     | 0     | 0   | 0   | 60   | 1    |
      | trna/incomplete_set | trna/two_alanine_ggc | ala_tRNA | -60   | -1    | 0   | 0   | 60   | 1    |
