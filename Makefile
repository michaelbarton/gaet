# These have to be absolute because sometimes process operates in ./tmp/* subdirs
path = PATH=$(abspath vendor/bin):$(abspath $(shell echo ./.tox/py35-build/bin)):${PATH}

.PHONY: build bootstrap doc test feature publish publish-binary publish-pypi

# dist used to named using python setup.py --version but there was a circular
# dependency with having a tox environment setup, using grep is also faster
# for the Makefile start up anyway
VERSION       = $(shell egrep -oh '[0-9]*\.[0-9]*\.[0-9]*' gaet/version.py)
COMPILED_DIR  = dist/binary/gaet-$(VERSION)
PYPI_DIR      = dist/package/gaet-$(VERSION)
MAN-PAGES     = $(COMPILED_DIR)/share/man/gaet.1 $(COMPILED_DIR)/share/man/gaet-format.5

TOX_CREATED_PACKAGE = ./.tox/dist/gaet-$(VERSION).zip
PYTHON_PACKAGE      = $(PYPI_DIR)/gaet-$(VERSION).tar.gz
COMPILED_PACKAGE    = $(COMPILED_DIR).tar.xz

###########################################
#
# Publish pre-compiled binary and pypi package
#
###########################################

deploy = $(path) \
	 aws s3 cp \
	 $(1)  \
	 s3://gaet/releases/$(2).tar.xz \
	 --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers


all: feature

publish: publish-binary publish-pypi

publish-pypi: $(PYTHON_PACKAGE)
	$(path) twine upload $<

publish-binary: $(COMPILED_PACKAGE)
	$(call deploy,$<,$(VERSION))
	$(call deploy,$<,latest)


###########################################
#
# Testing
#
###########################################

feature: test $(COMPILED_DIR)/bin/gaet
	$(path) TMPDIR=$(abspath tmp/aruba) cucumber $(FEATURES)

test:
	tox -e py35-unit -- $(ARGS)

###########################################
#
# Build python package
#
###########################################

$(PYTHON_PACKAGE): $(TOX_CREATED_PACKAGE)
	$(path) zip2tar --gz $<
	mkdir -p $(dir $@)
	mv $(basename $(TOX_CREATED_PACKAGE)).tar.gz $@

# A python package is created as part of the build process
$(TOX_CREATED_PACKAGE): $(COMPILED_DIR)/bin/gaet

###########################################
#
# Build project binary and documentation
#
###########################################

build: $(COMPILED_DIR)/bin/gaet $(MAN-PAGES)

$(COMPILED_DIR).tar.xz: $(COMPILED_DIR)/bin/gaet $(MAN-PAGES)
	tar -cJf $@ $(COMPILED_DIR)

$(COMPILED_DIR)/bin/gaet: $(shell find gaet bin -type f  ! -iname "*.pyc") requirements/default.txt setup.py MANIFEST.in
	tox -e py35-build -- $(dir $@)
	# Ensure timestamp is later than binary file to prevent circuar
	# dependency in Makefile targets
	touch $(TOX_CREATED_PACKAGE)

docs: $(MAN-PAGES)

$(COMPILED_DIR)/share/man/%: doc/src/%.md Gemfile.lock
	mkdir -p $(dir $@)
	$(path) ronn \
		--roff \
		--pipe \
		--organization="DOE Joint Genome Institute" \
		--manual="Gene-based Assembly Evaluation Tool" \
		$< > $@


###########################################
#
# Bootstrap project requirements
#
###########################################

bootstrap: \
	Gemfile.lock \
	tmp/tests \
	tmp/zoo_cache \
	test/data/bacteria.gff \
	test/data/archea.gff

test/data/%.gff: test/data/%.gff.xz
	xz --keep --force --decompress $<

tmp/tests:
	mkdir -p $@

tmp/zoo_cache:
	mkdir -p $@

tmp/input/archea.gz:
	mkdir -p $(dir $@)
	wget 'ftp://ftp.ensemblgenomes.org/pub/release-32/bacteria/fasta/bacteria_0_collection/archaeoglobus_fulgidus_dsm_4304/dna/Archaeoglobus_fulgidus_dsm_4304.ASM866v1.dna.chromosome.Chromosome.fa.gz' -O $@


tmp/input/bacteria.gz:
	mkdir -p $(dir $@)
	wget 'ftp://ftp.ensemblgenomes.org/pub/release-32/bacteria//fasta/bacteria_0_collection/escherichia_coli_str_k_12_substr_mg1655/dna/Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa.gz' -O $@


Gemfile.lock: Gemfile
	mkdir -p log
	bundle install --path vendor --binstubs vendor/bin 2>&1 > log/bundle.txt


clean:
	rm -rf tmp dist
